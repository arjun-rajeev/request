package com.cigniti.request;

import java.net.URI;

import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

/**
 * The class is an extenstion to the HttpEntityEnclosingRequestBase to provide the user with a custom HTTP delete
 * request with payload
 * 
 * @author Arjun.Rajeev
 */
@NotThreadSafe
class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase
{
	public static final String METHOD_NAME = "DELETE";
	
	public String getMethod()
	{
		return METHOD_NAME;
	}
	
	public HttpDeleteWithBody(final String uri)
	{
		super();
		setURI(URI.create(uri));
	}
	
	public HttpDeleteWithBody(final URI uri)
	{
		super();
		setURI(uri);
	}
	
	public HttpDeleteWithBody()
	{
		super();
	}
}
