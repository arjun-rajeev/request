package com.cigniti.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * This class will help the user send a REST DELETE request with a json payload
 * 
 * @author Arjun.Rajeev
 */
public class Request
{
	@SuppressWarnings("unused")
	private RequestMethod httpRequestMethod;
	private String serviceEndPoint;
	private String authString;
	private String payload;
	private HashMap<String, String> headers;
	
	public Request()
	{
		httpRequestMethod = RequestMethod.DELETEWITHPAYLOAD;
		headers = new HashMap<String, String>();
	}
	
	/**
	 * @param header
	 * @return the header value
	 */
	public String getHeader(String header)
	{
		return headers.get(header);
	}
	
	/**
	 * @param header
	 * @param value
	 * the headers to set
	 */
	public void setHeader(String header, String value)
	{
		headers.put(header, value);
	}
	
	/**
	 * @return the serviceEndPoint
	 */
	public String getServiceEndPoint()
	{
		return serviceEndPoint;
	}
	
	/**
	 * @param serviceEndPoint
	 * the serviceEndPoint to set
	 */
	public void setServiceEndPoint(String serviceEndPoint)
	{
		this.serviceEndPoint = serviceEndPoint;
	}
	
	/**
	 * @return the authString
	 */
	public String getAuthString()
	{
		return authString;
	}
	
	/**
	 * @param authString
	 * the authString to set
	 */
	public void setAuthString(String authString)
	{
		this.authString = authString;
	}
	
	/**
	 * @return the payload
	 */
	public String getPayload()
	{
		return payload;
	}
	
	/**
	 * @param payload
	 * the payload to set
	 */
	public void setPayload(String payload)
	{
		this.payload = payload;
	}
	
	/**
	 * The method sends out the actual delete request
	 * 
	 * @return String Response text with other parameters
	 */
	public String deleteWithPayload()
	{
		StringBuilder out = new StringBuilder();;
		String jsonForDelete = payload;
		int otherParam = 0;
		String otherParam2 = "";
		try
		{
			HttpEntity entity = new StringEntity(jsonForDelete);
			HttpClient httpClient = new DefaultHttpClient();
			HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(serviceEndPoint);
			httpDeleteWithBody.setHeader("Authorization", authString);
			httpDeleteWithBody.setHeader("Content-Type", "application/json");
			for (String key : headers.keySet())
			{
				httpDeleteWithBody.setHeader(key, getHeader(key));
			}
			httpDeleteWithBody.setEntity(entity);
			HttpResponse response = httpClient.execute(httpDeleteWithBody);
			otherParam = response.getStatusLine().getStatusCode();
			otherParam2 = response.getStatusLine().getReasonPhrase();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line;
			while ((line = reader.readLine()) != null)
			{
				out.append(line);
			}
			reader.close();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return "RESPONSE STATUS : " + otherParam2 + "\nRESPONSE CODE : " + Integer.toString(otherParam) + "\nRESPONSE TEXT : " + out.toString();
	}
}
