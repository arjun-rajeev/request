# README #

The request api is a simple example of extending the apache http core classes, when we requrie custom request types otherwise not supported by these frameworks.

### What is this repository for? ###

This repository is intended to help folks who want to send a delete request with a json payload using soapUI. By default soap ui wont allow its users to perform this. This is mainly due to the restrictions posed by the http core components (apache) used internally by soap UI. 

This framework was designed with one requirement in mind and may not cover all the edge cases. It is suggested that the users should perform thorough testing before making any commitments. 

### How do I get set up? ###

This is a maven based repo, and can be set up like any maven based projects

### How to use this with soap UI? ###

Build the jar from using maven. Copy the jar and its dependencies directly under the ext/bin folder of soapUI. 
Restart soapUI and create a groovy script test step and add the following snippet

```
#!groovy

import com.cigniti.request.Request;

Request deleteRequest = new Request();
deleteRequest.setServiceEndPoint("<serviceEndPointUrl>");
deleteRequest.setAuthString("Basic <base64encodedUserNameandPassord>");
deleteRequest.setPayload("{\"<key>\" : \"<value>\", \"key2\" : \"<value>\"}"); //only double quotes are allowed. 
String x = deleteRequest.deleteWithPayload();
log.info(x)
```

### Contribution guidelines ###

Everyone interested is free to create issues
For code contributions use fork and pull model

### Who do I talk to? ###

@arjajje